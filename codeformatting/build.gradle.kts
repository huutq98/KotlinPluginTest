plugins {
    kotlin("jvm") version "1.9.21"
}

group = "com.github"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib:1.9.21")
}

tasks.test {
    useJUnitPlatform()
}
kotlin {
    jvmToolchain(17)
}