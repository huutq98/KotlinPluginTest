package com.github.locator

import com.github.internal.SyntaxTokens.COMMENT_DELIMITERS
import com.github.internal.indicesOf
import com.github.internal.lengthToEOF
import com.github.model.PhraseLocation

internal object CommentLocator {

    fun locate(code: String): List<PhraseLocation> {
        val locations = mutableListOf<PhraseLocation>()
        val indices = mutableListOf<Int>()
        COMMENT_DELIMITERS.forEach { delimiter ->
            indices.addAll(code.indicesOf(delimiter))
        }

        indices.forEach { start ->
            val end = start + code.lengthToEOF(start)
            locations.add(PhraseLocation(start, end))
        }
        return locations
    }
}