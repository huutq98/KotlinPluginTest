package com.github.locator

import com.github.internal.SyntaxTokens.MARK_CHARACTERS
import com.github.internal.indicesOf
import com.github.model.PhraseLocation

internal object MarkLocator {
    fun locate(code: String): List<PhraseLocation> {
        val locations = mutableListOf<PhraseLocation>()
        code.asSequence()
            .toSet()
            .filter { it.toString() in MARK_CHARACTERS }
            .forEach {
                code.indicesOf(it.toString()).forEach { index ->
                    locations.add(PhraseLocation(index, index + 1))
                }
            }

        return locations
    }
}