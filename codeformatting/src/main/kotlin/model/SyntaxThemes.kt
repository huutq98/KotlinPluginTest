package model

private const val DARCULA_KEY = "darcula"
private const val MONOKAI_KEY = "monokai"
private const val NOTEPAD_KEY = "notepad"
private const val MATRIX_KEY = "matrix"
private const val PASTEL_KEY = "pastel"

object SyntaxThemes {

    val dark = mapOf(
        DARCULA_KEY to SyntaxTheme(
            key = DARCULA_KEY,
            code = 0xFFEDEDED,
            keyword = 0xFFCC7832,
            string = 0xFF6A8759,
            literal = 0xFF6897BB,
            comment = 0xFF909090,
            metadata = 0xFFBBB529,
            multilineComment = 0xFF629755,
            punctuation = 0xFFCC7832,
            mark = 0xFFEDEDED
        ),
        MONOKAI_KEY to SyntaxTheme(
            key = MONOKAI_KEY,
            code = 0xFFF8F8F2,
            keyword = 0xFFF92672,
            string = 0xFFE6DB74,
            literal = 0xFFAE81FF,
            comment = 0xFFFD971F,
            metadata = 0xFFB8F4B8,
            multilineComment = 0xFFFD971F,
            punctuation = 0xFFF8F8F2,
            mark = 0xFFF8F8F2
        ),
        NOTEPAD_KEY to SyntaxTheme(
            key = NOTEPAD_KEY,
            code = 0xFF000080,
            keyword = 0xFF0000FF,
            string = 0xFF808080,
            literal = 0xFFFF8000,
            comment = 0xFF008000,
            metadata = 0xFF000080,
            multilineComment = 0xFF008000,
            punctuation = 0xFFAA2C8C,
            mark = 0xFFAA2C8C
        ),
        MATRIX_KEY to SyntaxTheme(
            key = MATRIX_KEY,
            code = 0xFF008500,
            keyword = 0xFF008500,
            string = 0xFF269926,
            literal = 0xFF39E639,
            comment = 0xFF67E667,
            metadata = 0xFF008500,
            multilineComment = 0xFF67E667,
            punctuation = 0xFF008500,
            mark = 0xFF008500
        ),
        PASTEL_KEY to SyntaxTheme(
            key = PASTEL_KEY,
            code = 0xFFDFDEE0,
            keyword = 0xFF729FCF,
            string = 0xFF93CF55,
            literal = 0xFF8AE234,
            comment = 0xFF888A85,
            metadata = 0xFF5DB895,
            multilineComment = 0xFF888A85,
            punctuation = 0xFFCB956D,
            mark = 0xFFCB956D
        )
    )

    val light = mapOf(
        DARCULA_KEY to SyntaxTheme(
            key = DARCULA_KEY,
            code = 0xFF121212,
            keyword = 0xFFCC7832,
            string = 0xFF6A8759,
            literal = 0xFF6897BB,
            comment = 0xFF909090,
            metadata = 0xFFBBB529,
            multilineComment = 0xFF629755,
            punctuation = 0xFFCC7832,
            mark = 0xFF121212
        ),
        MONOKAI_KEY to SyntaxTheme(
            key = MONOKAI_KEY,
            code = 0xFF07070D,
            keyword = 0xFFF92672,
            string = 0xFFE6DB74,
            literal = 0xFFAE81FF,
            comment = 0xFFFD971F,
            metadata = 0xFFB8F4B8,
            multilineComment = 0xFFFD971F,
            punctuation = 0xFF07070D,
            mark = 0xFF07070D
        ),
        NOTEPAD_KEY to SyntaxTheme(
            key = NOTEPAD_KEY,
            code = 0xFF000080,
            keyword = 0xFF0000FF,
            string = 0xFF808080,
            literal = 0xFFFF8000,
            comment = 0xFF008000,
            metadata = 0xFF000080,
            multilineComment = 0xFF008000,
            punctuation = 0xFFAA2C8C,
            mark = 0xFFAA2C8C
        ),
        MATRIX_KEY to SyntaxTheme(
            key = MATRIX_KEY,
            code = 0xFF008500,
            keyword = 0xFF008500,
            string = 0xFF269926,
            literal = 0xFF39E639,
            comment = 0xFF67E667,
            metadata = 0xFF008500,
            multilineComment = 0xFF67E667,
            punctuation = 0xFF008500,
            mark = 0xFF008500
        ),
        PASTEL_KEY to SyntaxTheme(
            key = PASTEL_KEY,
            code = 0xFF20211F,
            keyword = 0xFF729FCF,
            string = 0xFF93CF55,
            literal = 0xFF8AE234,
            comment = 0xFF888A85,
            metadata = 0xFF5DB895,
            multilineComment = 0xFF888A85,
            punctuation = 0xFFCB956D,
            mark = 0xFFCB956D
        )
    )

    fun themes(darkMode: Boolean = false) = if (darkMode) dark else light

    fun default(darkMode: Boolean = false) = themes(darkMode)[DARCULA_KEY]!!

    fun darcula(darkMode: Boolean = false) = themes(darkMode)[DARCULA_KEY]!!
    fun monokai(darkMode: Boolean = false) = themes(darkMode)[MONOKAI_KEY]!!
    fun notepad(darkMode: Boolean = false) = themes(darkMode)[NOTEPAD_KEY]!!
    fun matrix(darkMode: Boolean = false) = themes(darkMode)[MATRIX_KEY]!!
    fun pastel(darkMode: Boolean = false) = themes(darkMode)[PASTEL_KEY]!!

    fun getNames(): List<String> = light.map {
        it.key
            .lowercase()
            .replaceFirstChar { if (it.isLowerCase()) it.titlecase() else it.toString() }
    }

    fun SyntaxTheme.useDark(darkMode: Boolean) = if (darkMode) dark[key] else light[key]
}