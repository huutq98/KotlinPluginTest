package model

enum class SyntaxLanguage {
    DEFAULT,
    MIXED,
    C,
    CPP,
    JAVASCRIPT,
    JAVA,
    KOTLIN,
    RUST,
    CSHARP,
    COFFEESCRIPT,
    PERL,
    PYTHON,
    RUBY,
    SHELL,
    SWIFT;

    companion object {
        fun getNames(): List<String> = entries.map {
            it.name.lowercase()
        }

        fun getByName(name: String): SyntaxLanguage? =
            entries.find { it.name.equals(name, ignoreCase = true) }
    }
}