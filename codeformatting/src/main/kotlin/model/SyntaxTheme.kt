package model

data class SyntaxTheme(
    val key: String,
    val code: Long,
    val keyword: Long,
    val string: Long,
    val literal: Long,
    val comment: Long,
    val metadata: Long,
    val multilineComment: Long,
    val punctuation: Long,
    val mark: Long
) {
    companion object {
        fun simple(key: String, code: Long, string: Long, accent: Long, value: Long) = SyntaxTheme(
            key = key,
            code = code,
            keyword = accent,
            string = string,
            literal = value,
            comment = string,
            metadata = value,
            multilineComment = string,
            punctuation = accent,
            mark = code
        )

        fun basic(key: String, code: Long, string: Long, accent: Long, value: Long, comment: Long) =
            SyntaxTheme(
                key = key,
                code = code,
                keyword = accent,
                string = string,
                literal = value,
                comment = comment,
                metadata = code,
                multilineComment = comment,
                punctuation = accent,
                mark = code
            )
    }
}