package com.github

import com.github.internal.CodeAnalyzer
import com.github.internal.CodeSnapshot
import com.github.model.*
import model.SyntaxLanguage
import model.SyntaxTheme
import model.SyntaxThemes

class Highlights private constructor(
    private var code: String,
    private val language: SyntaxLanguage,
    private val theme: SyntaxTheme,
    private var emphasisLocations: List<PhraseLocation>
) {
    private var snapshot: CodeSnapshot? = null

    companion object {
        fun default() = fromBuilder(Builder())

        fun fromBuilder(builder: Builder) = builder.build()

        fun themes(darkMode: Boolean) = SyntaxThemes.themes(darkMode)

        fun languages() = SyntaxLanguage.values().toList()
    }

    data class Builder(
        var code: String = "",
        var language: SyntaxLanguage = SyntaxLanguage.DEFAULT,
        var theme: SyntaxTheme = SyntaxThemes.default(),
        var emphasisLocations: List<PhraseLocation> = emptyList(),
    ) {
        fun code(code: String) = apply { this.code = code }
        fun language(language: SyntaxLanguage) = apply { this.language = language }
        fun theme(theme: SyntaxTheme) = apply { this.theme = theme }
        fun emphasis(vararg locations: PhraseLocation) =
            apply { this.emphasisLocations = locations.toList() }

        fun build() = Highlights(code, language, theme, emphasisLocations)
    }

    fun setCode(code: String) {
        this.code = code
    }

    fun setEmphasis(vararg locations: PhraseLocation) {
        this.emphasisLocations = locations.toList()
    }

    private fun getCodeStructure(): CodeStructure {
        val structure = CodeAnalyzer.analyze(code, language, snapshot)
        snapshot = CodeSnapshot(code, structure, language)
        return structure
    }

    fun getHighlights(): List<CodeHighlight> {
        val highlights = mutableListOf<CodeHighlight>()
        val structure = getCodeStructure()
        with(structure) {
            marks.forEach { highlights.add(ColorHighlight(it, theme.mark)) }
            punctuations.forEach { highlights.add(ColorHighlight(it, theme.punctuation)) }
            keywords.forEach { highlights.add(ColorHighlight(it, theme.keyword)) }
            strings.forEach { highlights.add(ColorHighlight(it, theme.string)) }
            literals.forEach { highlights.add(ColorHighlight(it, theme.literal)) }
            annotations.forEach { highlights.add(ColorHighlight(it, theme.metadata)) }
            comments.forEach { highlights.add(ColorHighlight(it, theme.comment)) }
            multilineComments.forEach { highlights.add(ColorHighlight(it, theme.multilineComment)) }
        }

        emphasisLocations.forEach { highlights.add(BoldHighlight(it)) }

        return highlights
    }

    fun getBuilder() = Builder(code, language, theme, emphasisLocations)

    fun getCode() = code

    fun getLanguage() = language

    fun getTheme() = theme

    fun getEmphasis() = emphasisLocations
}
