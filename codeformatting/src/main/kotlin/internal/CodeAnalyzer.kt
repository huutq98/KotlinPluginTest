package com.github.internal

import com.github.model.CodeStructure
import model.SyntaxLanguage
import com.github.internal.SyntaxTokens.ALL_KEYWORDS
import com.github.internal.SyntaxTokens.ALL_MIXED_KEYWORDS
import com.github.internal.SyntaxTokens.COFFEE_KEYWORDS
import com.github.internal.SyntaxTokens.CPP_KEYWORDS
import com.github.internal.SyntaxTokens.CSHARP_KEYWORDS
import com.github.internal.SyntaxTokens.C_KEYWORDS
import com.github.internal.SyntaxTokens.JAVA_KEYWORDS
import com.github.internal.SyntaxTokens.JSCRIPT_KEYWORDS
import com.github.internal.SyntaxTokens.KOTLIN_KEYWORDS
import com.github.internal.SyntaxTokens.PERL_KEYWORDS
import com.github.internal.SyntaxTokens.PYTHON_KEYWORDS
import com.github.internal.SyntaxTokens.RUBY_KEYWORDS
import com.github.internal.SyntaxTokens.RUST_KEYWORDS
import com.github.internal.SyntaxTokens.SH_KEYWORDS
import com.github.internal.SyntaxTokens.SWIFT_KEYWORDS
import com.github.locator.AnnotationLocator
import com.github.locator.CommentLocator
import com.github.locator.KeywordLocator
import com.github.locator.MarkLocator
import com.github.locator.MultilineCommentLocator
import com.github.locator.NumericLiteralLocator
import com.github.locator.PunctuationLocator
import com.github.locator.StringLocator

data class CodeSnapshot(
    val code: String,
    val structure: CodeStructure,
    val language: SyntaxLanguage,
)

internal object CodeAnalyzer {
    fun analyze(
        code: String,
        language: SyntaxLanguage = SyntaxLanguage.DEFAULT,
        snapshot: CodeSnapshot? = null,
    ): CodeStructure =
        when {
            snapshot == null -> analyzeFull(code, language)
            language != snapshot.language -> analyzeFull(code, language)
            code != snapshot.code -> analyzePartial(snapshot, code)
            else -> snapshot.structure
        }

    private fun analyzeFull(code: String, language: SyntaxLanguage): CodeStructure {
        return analyzeForLanguage(code, language)
    }

    private fun analyzePartial(codeSnapshot: CodeSnapshot, code: String): CodeStructure {
        val structure = when (val difference = CodeComparator.difference(codeSnapshot.code, code)) {
            is CodeDifference.Increase -> {
                val newStructure = analyzeForLanguage(difference.change, codeSnapshot.language)
                codeSnapshot.structure + newStructure.move(codeSnapshot.code.length + 1)
            }

            is CodeDifference.Decrease -> {
                val newStructure = analyzeForLanguage(difference.change, codeSnapshot.language)
                val lengthDifference = codeSnapshot.code.length - difference.change.length
                codeSnapshot.structure - newStructure.move(lengthDifference)
            }

            CodeDifference.None -> return codeSnapshot.structure
        }

        return structure
    }

    private fun analyzeForLanguage(code: String, language: SyntaxLanguage) =
        when (language) {
            SyntaxLanguage.DEFAULT -> analyzeCodeWithKeywords(code, ALL_KEYWORDS)
            SyntaxLanguage.MIXED -> analyzeCodeWithKeywords(code, ALL_MIXED_KEYWORDS)
            SyntaxLanguage.C -> analyzeCodeWithKeywords(code, C_KEYWORDS)
            SyntaxLanguage.CPP -> analyzeCodeWithKeywords(code, CPP_KEYWORDS)
            SyntaxLanguage.JAVA -> analyzeCodeWithKeywords(code, JAVA_KEYWORDS)
            SyntaxLanguage.KOTLIN -> analyzeCodeWithKeywords(code, KOTLIN_KEYWORDS)
            SyntaxLanguage.RUST -> analyzeCodeWithKeywords(code, RUST_KEYWORDS)
            SyntaxLanguage.CSHARP -> analyzeCodeWithKeywords(code, CSHARP_KEYWORDS)
            SyntaxLanguage.COFFEESCRIPT -> analyzeCodeWithKeywords(code, COFFEE_KEYWORDS)
            SyntaxLanguage.JAVASCRIPT -> analyzeCodeWithKeywords(code, JSCRIPT_KEYWORDS)
            SyntaxLanguage.PERL -> analyzeCodeWithKeywords(code, PERL_KEYWORDS)
            SyntaxLanguage.PYTHON -> analyzeCodeWithKeywords(code, PYTHON_KEYWORDS)
            SyntaxLanguage.RUBY -> analyzeCodeWithKeywords(code, RUBY_KEYWORDS)
            SyntaxLanguage.SHELL -> analyzeCodeWithKeywords(code, SH_KEYWORDS)
            SyntaxLanguage.SWIFT -> analyzeCodeWithKeywords(code, SWIFT_KEYWORDS)
        }

    private fun analyzeCodeWithKeywords(code: String, keywords: List<String>): CodeStructure {
        val comments = CommentLocator.locate(code)
        val multiLineComments = MultilineCommentLocator.locate(code)
        val strings = StringLocator.locate(code)

        val plainTextRanges = comments + multiLineComments + strings

        return CodeStructure(
            marks = MarkLocator.locate(code),
            punctuations = PunctuationLocator.locate(code),
            keywords = KeywordLocator.locate(code, keywords, plainTextRanges),
            strings = strings,
            literals = NumericLiteralLocator.locate(code),
            comments = comments,
            multilineComments = multiLineComments,
            annotations = AnnotationLocator.locate(code),
            incremental = false,
        )
    }
}