package com.github.jasontranz.kotlinplugintest.data.api

import com.github.jasontranz.kotlinplugintest.data.entity.request.ChatCompletionRequest
import com.github.jasontranz.kotlinplugintest.data.entity.response.ChatCompletionResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface ChatGPTAPI {
    @POST("completions")
    suspend fun getCompletion(@Body request: ChatCompletionRequest): ChatCompletionResponse
}