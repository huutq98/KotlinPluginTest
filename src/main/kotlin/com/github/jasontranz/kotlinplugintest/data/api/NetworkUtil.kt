package com.github.jasontranz.kotlinplugintest.data.api

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object NetworkUtil {
    fun retrofitInstance(apiKey: String): Retrofit {
        val client = OkHttpClient().newBuilder()
            .addInterceptor(MyInterceptor(apiKey))
            .build()

        return Retrofit.Builder()
            .client(client)
            .baseUrl("https://api.openai.com/v1/chat/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}

class MyInterceptor(private val apiKey: String): Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
            .newBuilder()
            .addHeader("Content-Type", "application/json")
            .addHeader("Authorization", "Bearer $apiKey")
            .build()
        return chain.proceed(request)
    }
}