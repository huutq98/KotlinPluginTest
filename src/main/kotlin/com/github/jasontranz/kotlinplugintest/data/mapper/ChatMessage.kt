package com.github.jasontranz.kotlinplugintest.data.mapper

import com.github.jasontranz.kotlinplugintest.data.entity.response.ChatMessageResponse
import com.github.jasontranz.kotlinplugintest.domain.model.ChatMessageData

fun ChatMessageResponse.toData(): ChatMessageData {
    return ChatMessageData(
        content = this.content ?: "",
        role = this.role ?: "user"
    )
}