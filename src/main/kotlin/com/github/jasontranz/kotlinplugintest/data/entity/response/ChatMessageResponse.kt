package com.github.jasontranz.kotlinplugintest.data.entity.response

import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName

data class ChatMessageResponse(
    @SerializedName("content") val content: String?,
    @SerializedName("role") val role: String?
) {
    constructor(json: JsonObject) : this(
        json["content"].asString ?: "Sorry, no answer found",
        json["role"].asString ?: "system"
    )
}
