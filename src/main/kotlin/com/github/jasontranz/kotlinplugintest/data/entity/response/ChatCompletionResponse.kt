package com.github.jasontranz.kotlinplugintest.data.entity.response

import com.google.gson.JsonObject

data class ChatCompletionResponse(
    val id: String?,
    val created: Long?,
    val choices: List<ChatCompletionChoiceResponse>?,
) {
    constructor(json: JsonObject) : this(
        json["id"].asString,
        json["created"].asLong,
        json["choices"].asJsonArray.map { ChatCompletionChoiceResponse(it.asJsonObject) },
    )
}

data class ChatCompletionChoiceResponse(
    val index: Int?,
    val message: ChatMessageResponse?,
    val finishReason: String?
) {

    constructor(json: JsonObject) : this(
        json["index"].asInt,
        ChatMessageResponse(json["message"].asJsonObject),
        if (json["finish_reason"].isJsonNull) "" else json["finish_reason"].asString
    )
}
