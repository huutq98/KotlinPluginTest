package com.github.jasontranz.kotlinplugintest.data.service

import com.github.jasontranz.kotlinplugintest.data.api.ChatGPTAPI
import com.github.jasontranz.kotlinplugintest.data.api.NetworkUtil
import com.github.jasontranz.kotlinplugintest.data.entity.request.ChatCompletionRequest
import com.github.jasontranz.kotlinplugintest.data.entity.response.ChatCompletionChoiceResponse
import com.github.jasontranz.kotlinplugintest.data.entity.response.ChatCompletionResponse
import com.github.jasontranz.kotlinplugintest.data.local.AppSettingsState
import com.github.jasontranz.kotlinplugintest.domain.repository.ChatGptRepository
import com.google.gson.JsonObject
import com.intellij.openapi.diagnostic.thisLogger
import kotlinx.coroutines.flow.Flow
import retrofit2.Retrofit


class ChatGPTService : ChatGPTAPI {
    private val service: ChatGPTAPI by lazy {
        NetworkUtil.retrofitInstance("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").create(ChatGPTAPI::class.java)
    }

    override suspend fun getCompletion(request: ChatCompletionRequest): ChatCompletionResponse {
        try {
            return service.getCompletion(request)
        } catch (e: Exception) {
            thisLogger().error("ChatGPTService.getCompletion: $e")
            throw e
        }
    }
}
