package com.github.jasontranz.kotlinplugintest.data.entity.request

import com.google.gson.annotations.SerializedName

data class ChatMessageRequest(
    @SerializedName("content") val content: String?,
    @SerializedName("role") val role: String?
)