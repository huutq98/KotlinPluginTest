package com.github.jasontranz.kotlinplugintest.data.repository

import com.github.jasontranz.kotlinplugintest.data.entity.response.ChatCompletionResponse
import com.github.jasontranz.kotlinplugintest.data.mapper.toRequest
import com.github.jasontranz.kotlinplugintest.data.service.ChatGPTService
import com.github.jasontranz.kotlinplugintest.domain.model.ChatCompletionRequestData
import com.github.jasontranz.kotlinplugintest.domain.repository.ChatGptRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow


class ChatGPTRepositoryImpl : ChatGptRepository {
    private val service: ChatGPTService = ChatGPTService()

    override fun getCompletion(request: ChatCompletionRequestData): Flow<ChatCompletionResponse> {
        return flow {
            emit(service.getCompletion(request.toRequest()))
        }
    }
}