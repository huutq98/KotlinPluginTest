package com.github.jasontranz.kotlinplugintest.data.mapper

import com.github.jasontranz.kotlinplugintest.data.entity.request.ChatCompletionRequest
import com.github.jasontranz.kotlinplugintest.data.entity.response.ChatCompletionChoiceResponse
import com.github.jasontranz.kotlinplugintest.data.entity.response.ChatCompletionResponse
import com.github.jasontranz.kotlinplugintest.domain.model.ChatCompletionChoiceData
import com.github.jasontranz.kotlinplugintest.domain.model.ChatCompletionData
import com.github.jasontranz.kotlinplugintest.domain.model.ChatCompletionRequestData

fun ChatCompletionResponse.toData(): ChatCompletionData {
    return ChatCompletionData(
        id = this.id,
        created = this.created,
        choices = this.choices?.map { it.toData() }
    )
}

fun ChatCompletionChoiceResponse.toData(): ChatCompletionChoiceData {
    return ChatCompletionChoiceData(
        index = this.index,
        message = this.message?.toData(),
        finishReason = this.finishReason
    )
}

fun ChatCompletionRequestData.toRequest(): ChatCompletionRequest {
    return ChatCompletionRequest(
        model = model,
        messages = messages,
        topP = topP,
        n = n,
        stream = stream,
        stop = stop,
        maxTokens = maxTokens,
        presencePenalty = presencePenalty,
        frequencyPenalty = frequencyPenalty,
        logitBias = logitBias
    )
}