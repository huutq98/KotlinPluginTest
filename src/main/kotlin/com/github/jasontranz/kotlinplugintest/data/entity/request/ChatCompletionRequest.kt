package com.github.jasontranz.kotlinplugintest.data.entity.request

import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName

data class ChatCompletionRequest(
    val model: String, // recommend: "gpt-3.5-turbo"
    val messages: MutableList<ChatMessageRequest>,
    private val temperature: Float = 0.0f,
    @SerializedName("top_p") val topP: Float = 0.0f,
    val n: Int = 1,
    val stream: Boolean = false,
    val stop: String? = null,
    @SerializedName("max_tokens") val maxTokens: Int? = null, // default is 4096
    @SerializedName("presence_penalty") val presencePenalty: Float = 0.0f,
    @SerializedName("frequency_penalty") val frequencyPenalty: Float = 0.0f,
    @SerializedName("logit_bias") val logitBias: JsonObject? = null,
    val user: String? = null
) {
    constructor(model: String, systemContent: String) : this(
        model,
        arrayListOf(ChatMessageRequest(systemContent, "system"))
    )

    fun addMessage(prompt: String) {
        this.messages.add(ChatMessageRequest(prompt,"user"))
    }
}