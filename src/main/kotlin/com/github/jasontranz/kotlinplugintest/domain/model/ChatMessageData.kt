package com.github.jasontranz.kotlinplugintest.domain.model

data class ChatMessageData(
    val content: String,
    val role: String = ChatMessageType.USER.role
)

enum class ChatMessageType(val role: String) {
    USER("user"),
    BOT("assistant")
}
