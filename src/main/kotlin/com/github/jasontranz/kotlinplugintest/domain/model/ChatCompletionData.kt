package com.github.jasontranz.kotlinplugintest.domain.model

data class ChatCompletionData(
    val id: String? = "",
    val created: Long? = 0L,
    val choices: List<ChatCompletionChoiceData>? = emptyList(),
)
