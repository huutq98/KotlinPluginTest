package com.github.jasontranz.kotlinplugintest.domain.repository

import com.github.jasontranz.kotlinplugintest.data.entity.response.ChatCompletionResponse
import com.github.jasontranz.kotlinplugintest.domain.model.ChatCompletionRequestData
import kotlinx.coroutines.flow.Flow

interface ChatGptRepository {
    fun getCompletion(request: ChatCompletionRequestData): Flow<ChatCompletionResponse>
}