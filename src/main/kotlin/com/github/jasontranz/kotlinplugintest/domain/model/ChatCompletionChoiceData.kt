package com.github.jasontranz.kotlinplugintest.domain.model

data class ChatCompletionChoiceData(
    val index: Int?,
    val message: ChatMessageData?,
    val finishReason: String?
)