package com.github.jasontranz.kotlinplugintest.domain.model

import com.github.jasontranz.kotlinplugintest.data.entity.request.ChatMessageRequest
import com.google.gson.JsonObject

data class ChatCompletionRequestData(
    val model: String = "gpt-3.5-turbo", // recommend: "gpt-3.5-turbo"
    val messages: MutableList<ChatMessageRequest>,
    private val temperature: Float = 0.0f,
    val topP: Float = 0.0f,
    val n: Int = 1,
    val stream: Boolean = false,
    val stop: String? = null,
    val maxTokens: Int? = null, // default is 4096
    val presencePenalty: Float = 0.0f,
    val frequencyPenalty: Float = 0.0f,
    val logitBias: JsonObject? = null,
    val user: String? = null
) {
    constructor(model: String, systemContent: String) : this(
        model,
        arrayListOf(ChatMessageRequest(systemContent, "system"))
    )

    fun addMessage(prompt: String) {
        this.messages.add(ChatMessageRequest(prompt,"user"))
    }
}
