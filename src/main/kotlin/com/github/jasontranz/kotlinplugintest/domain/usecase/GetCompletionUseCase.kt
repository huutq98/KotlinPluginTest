package com.github.jasontranz.kotlinplugintest.domain.usecase

import com.github.jasontranz.kotlinplugintest.data.mapper.toData
import com.github.jasontranz.kotlinplugintest.domain.model.ChatCompletionData
import com.github.jasontranz.kotlinplugintest.domain.model.ChatCompletionRequestData
import com.github.jasontranz.kotlinplugintest.domain.repository.ChatGptRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

interface GetCompletionUseCase {
    fun execute(request: ChatCompletionRequestData): Flow<ChatCompletionData>

}

class GetCompletionUseCaseImpl(
    private val repository: ChatGptRepository
): GetCompletionUseCase {
    override fun execute(request: ChatCompletionRequestData): Flow<ChatCompletionData> {
        return repository.getCompletion(request).map { it.toData() }
    }
}
