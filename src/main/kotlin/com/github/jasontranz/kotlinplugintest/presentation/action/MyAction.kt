package com.github.jasontranz.kotlinplugintest.presentation.action

import com.intellij.ide.BrowserUtil
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import java.util.*

class MyAction: AnAction() {
    override fun actionPerformed(e: AnActionEvent) {
        val file = e.getData(CommonDataKeys.PSI_FILE)
        val lang = e.getData(CommonDataKeys.PSI_FILE)?.language
        val languageTag = "+[" + lang?.displayName?.toLowerCase(Locale.getDefault()) + "]";

        val editor = e.getRequiredData(CommonDataKeys.EDITOR)
        val caretModel = editor.caretModel
        val selectedText = caretModel.currentCaret.selectedText

        val query = selectedText?.replace(' ', '+') + languageTag

        BrowserUtil.browse("https://stackoverflow.com/search?q=$query")
    }
}