package com.github.jasontranz.kotlinplugintest.presentation.component

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.BitmapPainter
import androidx.compose.ui.input.pointer.PointerIcon
import androidx.compose.ui.input.pointer.pointerHoverIcon
import androidx.compose.ui.unit.dp
import com.github.jasontranz.kotlinplugintest.domain.model.ChatMessageData
import com.github.jasontranz.kotlinplugintest.domain.model.ChatMessageType
import com.intellij.openapi.project.Project

@Composable
fun UserMessage(message: ChatMessageData, project: Project) {

    fun isBot(): Boolean {
        return message.role == ChatMessageType.BOT.role
    }

    val imageUrl = if (isBot()) {
        "https://cdn-icons-png.flaticon.com/512/8649/8649607.png"
    } else {
        "https://cdn.iconscout.com/icon/free/png-256/free-avatar-370-456322.png?f=webp"
    }

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 8.dp)
            .clip(RoundedCornerShape(8.dp))
            .border(1.dp, if (isBot()) Color(0xFF1E1F22) else Color.Transparent, RoundedCornerShape(8.dp))
            .background(if (isBot()) Color(0xFF1E1F22) else Color.Transparent)
            .padding(8.dp)
    ) {
        Image(
            painter = BitmapPainter(loadNetworkImage(imageUrl)),
            contentDescription = "Call",
            modifier = Modifier.size(40.dp).clip(CircleShape)
                .padding(end = 8.dp)
        )
        SelectionContainer {
            IntelliJStyleText(
                text = message.content,
                modifier = Modifier
                    .pointerHoverIcon(icon = PointerIcon.Hand),
                color = Color(0xFFCACCCF),
                project = project
            )
        }
    }
}

// need to put it when loading
@Composable
fun LoadingMessage() {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 8.dp)
            .clip(RoundedCornerShape(8.dp))
            .border(1.dp, Color(0xFF1E1F22), RoundedCornerShape(8.dp))
            .background(Color(0xFF1E1F22))
            .padding(8.dp)
    ) {
        Image(
            painter = BitmapPainter(loadNetworkImage("https://cdn-icons-png.flaticon.com/512/8649/8649607.png")),
            contentDescription = "Call",
            modifier = Modifier.size(40.dp).clip(CircleShape)
                .padding(end = 8.dp)
        )
    }
}