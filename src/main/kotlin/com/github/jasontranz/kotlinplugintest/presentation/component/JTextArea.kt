package com.github.jasontranz.kotlinplugintest.presentation.component

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.key.Key
import androidx.compose.ui.input.key.isShiftPressed
import androidx.compose.ui.input.key.key
import androidx.compose.ui.input.key.onPreviewKeyEvent
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import com.intellij.openapi.diagnostic.thisLogger


@Composable
fun searchView(
    modifier: Modifier = Modifier,
    onSearch: (String) -> Unit
) {
    val textValue = remember { mutableStateOf("") }
    val pointerFocus = FocusRequester()
    val heightTextField = remember { mutableStateOf(0) }
    val numberOfLine = remember { mutableStateOf(0) }

    fun onSearchClick() {
        onSearch(textValue.value)
        textValue.value = ""
    }

    fun calculateBonusHeight() = run {
        if (numberOfLine.value > 1) {
            5
        } else {
            0
        }.dp
    }

    Column(
        modifier = modifier
            .fillMaxWidth()
            .padding(8.dp)
            .clip(RoundedCornerShape(8.dp))
            .border(1.dp, Color.Gray, RoundedCornerShape(8.dp))
            .background(Color(0xFF1E1F22))
            .padding(8.dp)
            .noRippleClickable {
                pointerFocus.requestFocus()
            }
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .align(Alignment.CenterHorizontally)
        ) {
            BasicTextField(
                value = textValue.value,
                onValueChange = {
                    textValue.value = it
                },
                maxLines = Int.MAX_VALUE,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 16.dp)
                    .focusRequester(focusRequester = pointerFocus)
                    .onPreviewKeyEvent { keyEvent ->
                        if (keyEvent.isShiftPressed && keyEvent.key == Key.Enter) {
                            // Shift + Enter key event detected
                            return@onPreviewKeyEvent true
                        } else if (keyEvent.key == Key.Enter && !keyEvent.isShiftPressed) {
                            // Handle the "Enter" key action here
                            if (textValue.value.trim().isNotEmpty()) {
                                onSearchClick()
                            }
                            return@onPreviewKeyEvent true
                        }
                        false
                    }
                    .onGloballyPositioned {
                        heightTextField.value = it.size.height
                    },
                onTextLayout = { textLayoutResult ->
                    numberOfLine.value = textLayoutResult.lineCount
                },
                textStyle = TextStyle(color = Color(0xFFCACCCF))
            )
            Spacer(modifier = Modifier.height(calculateBonusHeight()))
        }

        Box(
            modifier = Modifier
                .clip(RoundedCornerShape(5.dp))
                .border(0.5.dp, Color.Gray, RoundedCornerShape(5.dp))
                .background(
                    color = if (textValue.value.isNotEmpty()) {
                        Color(0xFF3574F0)
                    } else {
                        Color(0xFF1E1F22)
                    }
                )
                .align(Alignment.End)
                .noRippleClickable {
                    onSearchClick()
                }
        ) {
            Text(
                text = "Search",
                color = Color(0xFFCACCCF),
                modifier = Modifier.padding(horizontal = 10.dp, vertical = 6.dp)
            )
        }
    }
}

//@Composable
//fun VerticalScrollbar(
//    modifier: Modifier = Modifier,
//    adapter: ScrollbarAdapter
//) {
//    Canvas(modifier = modifier.fillMaxHeight()) {
//        val scrollbarHeight = size.height
//        val scrollbarWidth = 8.dp.toPx()
//
//        val totalSize = adapter.contentSize
//        val firstVisibleItemIndex = 0
//
//        val visiblePart = if (totalSize > 0) {
//            1f - (firstVisibleItemIndex / (totalSize - 1f))
//        } else {
//            0f
//        }
//
//        val thumbHeight = scrollbarHeight * visiblePart.toFloat()
//        val thumbOffset = scrollbarHeight * adapter.scrollOffset
//
//        drawRect(
//            color = Color.Gray,
//            topLeft = Offset(size.width - scrollbarWidth, 0f),
//            size = Size(scrollbarWidth, scrollbarHeight)
//        )
//
//        drawRect(
//            color = Color.Blue,
//            topLeft = Offset(size.width - scrollbarWidth, thumbOffset.toFloat()),
//            size = Size(scrollbarWidth, thumbHeight)
//        )
//    }
//}
