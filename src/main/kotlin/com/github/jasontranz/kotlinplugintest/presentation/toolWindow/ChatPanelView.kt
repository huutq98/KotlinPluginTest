package com.github.jasontranz.kotlinplugintest.presentation.toolWindow

import androidx.compose.foundation.*
import androidx.compose.foundation.gestures.scrollBy
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.dp
import com.github.jasontranz.kotlinplugintest.presentation.action.ChatBotAction
import com.github.jasontranz.kotlinplugintest.presentation.component.AsyncImage
import com.github.jasontranz.kotlinplugintest.presentation.component.UserMessage
import com.github.jasontranz.kotlinplugintest.presentation.component.searchView
import com.intellij.openapi.project.Project
import kotlinx.coroutines.launch

@Composable
fun ChatPanelView(
    chatBotAction: ChatBotAction,
    project: Project
) {
    val chatMessages = chatBotAction.messageList.collectAsState()
    val loadingState = chatBotAction.loadingResponse.collectAsState()

    val searchViewHeight = remember { mutableStateOf(0.dp) }
    val lazyListState = rememberLazyListState()
    val scrollbarAdapter = rememberScrollbarAdapter(lazyListState)
    val coroutineScope = rememberCoroutineScope()

    val localDensity = LocalDensity.current

    Box(
        modifier = Modifier.fillMaxSize(),
    ) {
        if (chatMessages.value.isEmpty()) {
            EmptyView(
                modifier = Modifier
                    .align(Alignment.TopStart)
                    .padding(bottom = searchViewHeight.value)
            )
        } else {
            LazyColumn(
                modifier = Modifier
                    .align(Alignment.TopStart)
                    .padding(bottom = searchViewHeight.value)
                    .padding(end = 6.dp),
                state = lazyListState
            ) {
                items(
                    count = chatMessages.value.size,
                    itemContent = { index ->
                        val message = chatMessages.value[index]
                        UserMessage(message = message, project = project)
                    }
                )
            }

//            val composition by rememberLottieComposition(LottieCompositionSpec.JsonString(lottieData))

            VerticalScrollbar(
                adapter = scrollbarAdapter,
                modifier = Modifier
                    .align(Alignment.CenterEnd)
                    .fillMaxHeight()
                    .padding(bottom = searchViewHeight.value),
                style = ScrollbarStyle( // Style for scrollbar
                    minimalHeight = 16.dp,
                    thickness = 8.dp,
                    shape = RoundedCornerShape(8.dp),
                    hoverDurationMillis = 300,
                    unhoverColor = Color(0xFF1E1F22),
                    hoverColor = Color(0xFF595A5C)
                )
            )
        }

        searchView(
            modifier = Modifier.align(Alignment.BottomCenter)
                .onGloballyPositioned {
                    searchViewHeight.value = with(localDensity) { it.size.height.toDp() }
                }
        ) {
            chatBotAction.sendMessage(it)
        }
    }

    if (chatMessages.value.size > 0) {
        coroutineScope.launch {
            lazyListState.animateScrollToItem(chatMessages.value.size - 1)
        }
    }
}

@Composable
fun EmptyView(
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        AsyncImage(
            url = "https://cdn-icons-png.flaticon.com/512/8649/8649607.png",
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .size(80.dp)
                .clip(CircleShape)
        )

        Spacer(modifier = Modifier.height(10.dp))

        Column(
            modifier = Modifier
                .clip(RoundedCornerShape(8.dp))
                .border(1.dp, Color(0xFF548AF7), RoundedCornerShape(8.dp))
                .background(Color(0xFF1E1F22))
                .padding(8.dp)
        ) {
            Text(
                text = "Welcome to ChatBot",
                color = Color.White,
                modifier = Modifier.padding(8.dp)
            )
        }
    }
}