package com.github.jasontranz.kotlinplugintest.presentation.toolWindow

import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.thisLogger
import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowFactory
import com.intellij.ui.components.JBLabel
import com.intellij.ui.components.JBPanel
import com.intellij.ui.content.ContentFactory
import com.github.jasontranz.kotlinplugintest.MyBundle
import com.github.jasontranz.kotlinplugintest.data.service.MyProjectService
import com.intellij.ui.components.JBList
import com.intellij.ui.components.JBTextArea
import com.intellij.util.ui.GridBag
import java.awt.Dimension
import java.awt.GridBagConstraints
import java.awt.GridBagLayout
import javax.swing.BorderFactory
import javax.swing.JButton
import javax.swing.JComponent


class MyToolWindowFactory : ToolWindowFactory {

    init {
        thisLogger().warn("Don't forget to remove all non-needed sample code files with their corresponding registration entries in `plugin.xml`.")
    }

    override fun createToolWindowContent(project: Project, toolWindow: ToolWindow) {
        val myToolWindow = MyToolWindow(toolWindow)
        val content = ContentFactory.getInstance().createContent(myToolWindow.panelContent(), null, false)
        toolWindow.contentManager.addContent(content)
    }

    override fun shouldBeAvailable(project: Project) = true

    class MyToolWindow(toolWindow: ToolWindow) {

        private val service = toolWindow.project.service<MyProjectService>()

        fun panelContent(): JComponent {

            val textField = JBTextArea("Hello, World!").apply {
                preferredSize = Dimension(200, 200)
            }

            val gb = GridBag()
                .setDefaultWeightX(1.0)
                .setDefaultFill(GridBagConstraints.CENTER)

            val panel = JBPanel<JBPanel<*>>(GridBagLayout()).apply {
                preferredSize = Dimension(200, 400)
                add(JBLabel("Hello, World!"), gb.nextLine())
                add(JBLabel("Hello, World2!"), gb.nextLine())
                add(JBLabel("Hello, World3!"), gb.nextLine())
                add(textField, gb.nextLine())
                border = BorderFactory.createEmptyBorder(10, 10, 10, 10)
            }

            return panel
        }

        fun getContent() = JBPanel<JBPanel<*>>().apply {
            val jbList = JBList(arrayOf("Hello", "World"))
            val label = JBLabel(MyBundle.message("randomLabel", "?"))

//            add(label)
//            add(JButton(MyBundle.message("shuffle")).apply {
//                addActionListener {
//                    label.text = MyBundle.message("randomLabel", service.getRandomNumber())
//                }
//            })

//            preferredSize = Dimension(300, 200)

            val text = JBTextArea()
            text.add(JButton(MyBundle.message("shuffle")).apply {
                addActionListener {
                    text.text = MyBundle.message("randomLabel", service.getRandomNumber())
                }
            })
            text.preferredSize = Dimension(250, 400)
            add(text, "Center")
            add(jbList, "Bottom")
        }
    }
}
