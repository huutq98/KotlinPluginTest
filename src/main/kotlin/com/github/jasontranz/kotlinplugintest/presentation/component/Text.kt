package com.github.jasontranz.kotlinplugintest.presentation.component

import androidx.compose.ui.text.AnnotatedString
import com.github.Highlights
import com.github.model.CodeHighlight
import com.intellij.formatting.FormattingModelProvider
import com.intellij.openapi.fileTypes.FileType
import com.intellij.openapi.fileTypes.FileTypeManager
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiFile
import com.intellij.psi.PsiFileFactory
import com.intellij.psi.PsiManager
import com.intellij.psi.codeStyle.CodeStyleManager
import model.SyntaxLanguage
import model.SyntaxThemes
import com.intellij.formatting.FormattingModelBuilder
import com.intellij.openapi.editor.Document
import com.intellij.psi.PsiDocumentManager

object KotlinCodeFormatter {
    fun formatCode(code: String, project: Project): String {
        val psiFileFactory = PsiFileFactory.getInstance(project)
        val ktFileType: FileType = FileTypeManager.getInstance().getFileTypeByExtension("kt")
        val psiFile: PsiFile = psiFileFactory.createFileFromText("temp.kt", ktFileType, code)

        val codeStyleManager = CodeStyleManager.getInstance(project)
        codeStyleManager.reformat(psiFile)

        return psiFile.text
    }

    fun getCodeHighLights(code: String, language: SyntaxLanguage): List<CodeHighlight> {

        Highlights.Builder()
            .code(code)
            .theme(SyntaxThemes.default(true))
            .language(language)
            .build()
            .run {
                return getHighlights()
            }
    }

    fun formatCode1(code: String, project: Project): String {
        val psiFile = createPsiFileFromText(code, project)
        val psiDocumentManager = PsiDocumentManager.getInstance(project)
        val document = psiDocumentManager.getDocument(psiFile) ?: return code // Return original code if document is null

        // Perform code formatting
        CodeStyleManager.getInstance(project).reformat(psiFile)

        // Get the formatted text from the document
        return document.text
    }

    private fun createPsiFileFromText(code: String, project: Project): PsiFile {
        val psiFileFactory = PsiFileFactory.getInstance(project)
        val ktFileType: FileType = FileTypeManager.getInstance().getFileTypeByExtension("kt")
        return psiFileFactory.createFileFromText("temp.kt", ktFileType, code)
    }
}


