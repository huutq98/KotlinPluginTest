package com.github.jasontranz.kotlinplugintest.presentation.action

import com.github.jasontranz.kotlinplugintest.data.local.AppSettingsState
import com.github.jasontranz.kotlinplugintest.data.mapper.PromptFormatter
import com.github.jasontranz.kotlinplugintest.data.repository.ChatGPTRepositoryImpl
import com.github.jasontranz.kotlinplugintest.domain.model.ChatCompletionData
import com.github.jasontranz.kotlinplugintest.domain.model.ChatCompletionRequestData
import com.github.jasontranz.kotlinplugintest.domain.model.ChatMessageData
import com.github.jasontranz.kotlinplugintest.domain.model.ChatMessageType
import com.github.jasontranz.kotlinplugintest.domain.usecase.GetCompletionUseCaseImpl
import com.github.jasontranz.kotlinplugintest.presentation.toolWindow.ContentPanelComponent
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.diagnostic.thisLogger
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch


class ChatBotAction(private var actionType: ChatBotActionType) {
    private val chatGptRepository = ChatGPTRepositoryImpl()
    private val getCompletionUseCase = GetCompletionUseCaseImpl(chatGptRepository)

    private val _responseList: MutableStateFlow<List<ChatCompletionData>> = MutableStateFlow(mutableListOf())
    val responseList: StateFlow<List<ChatCompletionData>> = _responseList.asStateFlow()

    private val _messageList: MutableStateFlow<MutableList<ChatMessageData>> = MutableStateFlow(mutableListOf())
    val messageList: StateFlow<MutableList<ChatMessageData>> = _messageList.asStateFlow()

    private val _loadingResponse: MutableStateFlow<Boolean> = MutableStateFlow(false)
    val loadingResponse: StateFlow<Boolean> = _loadingResponse.asStateFlow()

    private val pluginScope = CoroutineScope(Dispatchers.IO + SupervisorJob())

    val action = if (actionType == ChatBotActionType.EXPLAIN) "explain" else "refactor"

    fun setActionType(actionType: ChatBotActionType) {
        this.actionType = actionType
    }

    fun getLabel(): String {
        val capitalizedAction = action.capitalize()
        return "$capitalizedAction Code"
    }

    private fun getCodeSection(content: String): String {
        val pattern = "```(.+?)```".toRegex(RegexOption.DOT_MATCHES_ALL)
        val match = pattern.find(content)

        if (match != null) return match.groupValues[1].trim()
        return ""
    }

    private fun makeChatBotRequest(prompt: String): ChatCompletionRequestData {
        val apiKey = AppSettingsState.instance.apiKey
        val model = AppSettingsState.instance.model.ifEmpty { "gpt-3.5-turbo" }

//        if (apiKey.isEmpty()) {
//            return
//        }

        val system = "Be as helpful as possible and concise with your response"
        val request = ChatCompletionRequestData(model, system)
        request.addMessage(prompt)
        return request
    }

    fun handlePromptAndResponse(
        ui: ContentPanelComponent,
        prompt: PromptFormatter,
        replaceSelectedText: ((response: String) -> Unit)? = null
    ) {
        ui.add(prompt.getUIPrompt(), true)
        ui.add("Loading...")

        ApplicationManager.getApplication().executeOnPooledThread {
            pluginScope.launch {
                getCompletionUseCase.execute(makeChatBotRequest(prompt.getRequestPrompt()))
                    .catch {
                        thisLogger().error("Error getting completion data", it)
                    }
                    .collect {
                        val response = it.choices?.getOrNull(0)?.message?.content ?: "Something wrong please try again"
                        ApplicationManager.getApplication().invokeLater {
                            when {
                                actionType === ChatBotActionType.REFACTOR -> ui.updateReplaceableContent(response) {
                                    replaceSelectedText?.invoke(getCodeSection(response))
                                }
                                else -> ui.updateMessage(response)
                            }
                        }
                    }
            }
        }
    }

    fun sendMessage(message: String) {

        addMessageToCurrentView(message, ChatMessageType.USER.role)

        val prompt = object : PromptFormatter {
            override fun getUIPrompt() = message
            override fun getRequestPrompt() = message
        }
        pluginScope.launch {
            getCompletionUseCase.execute(makeChatBotRequest(prompt.getRequestPrompt()))
                .onStart {
                    _loadingResponse.update { true }
                }
                .catch {
                    thisLogger().error("Error getting completion data", it)
                }
                .collect { result ->
                    println("result.choices: ${result.choices}")
                    val response = result.choices?.getOrNull(0)?.message?.content ?: "Something wrong please try again"
                    ApplicationManager.getApplication().invokeLater {
                        when {
                            actionType === ChatBotActionType.REFACTOR -> {
//                                ui.updateReplaceableContent(response) {
//                                    replaceSelectedText?.invoke(getCodeSection(response))
//                                }
                            }
                            else -> {
//                                addResponseToCurrentView(result)
                                addMessageToCurrentView(response, ChatMessageType.BOT.role)
                            }
                        }
                    }

                    _loadingResponse.update { false }
                }
        }
    }

    private fun addResponseToCurrentView(response: ChatCompletionData) {
        val responseList = mutableListOf<ChatCompletionData>()
        responseList.addAll(_responseList.value)
        responseList.add(response)
        _responseList.update { responseList }
    }

    private fun addMessageToCurrentView(message: String, role: String = "user") {
        val messageList = mutableListOf<ChatMessageData>()
        messageList.addAll(_messageList.value)
        messageList.add(ChatMessageData(message, role))
        _messageList.update { messageList }
    }
}

enum class ChatBotActionType {
    REFACTOR,
    EXPLAIN
}
