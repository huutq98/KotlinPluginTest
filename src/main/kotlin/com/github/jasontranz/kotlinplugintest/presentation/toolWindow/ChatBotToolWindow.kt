package com.github.jasontranz.kotlinplugintest.presentation.toolWindow

import com.github.jasontranz.kotlinplugintest.presentation.action.ChatBotAction
import com.github.jasontranz.kotlinplugintest.presentation.action.ChatBotActionType
import com.github.jasontranz.kotlinplugintest.presentation.setting.AppSettingsComponent
import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowFactory
import com.intellij.ui.content.ContentFactory

class ChatBotToolWindow : ToolWindowFactory {
    override fun createToolWindowContent(project: Project, toolWindow: ToolWindow) {
        val contentFactory = ContentFactory.getInstance()
        val chatBotActionService = ChatBotAction(ChatBotActionType.EXPLAIN)
        val contentPanel = ContentPanelComponent(chatBotAction = chatBotActionService, project = project)
        val createContent = contentFactory?.createContent(contentPanel, "", true)
        toolWindow.contentManager.addContent(createContent!!)
    }
}
