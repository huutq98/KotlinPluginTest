package com.github.jasontranz.kotlinplugintest.presentation.component

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import com.github.jasontranz.kotlinplugintest.presentation.component.KotlinCodeFormatter.formatCode1
import com.github.jasontranz.kotlinplugintest.presentation.component.KotlinCodeFormatter.getCodeHighLights
import com.github.model.ColorHighlight
import com.intellij.openapi.project.Project
import model.SyntaxLanguage

@Composable
fun IntelliJStyleText(
    modifier: Modifier = Modifier,
    text: String = "",
    color: Color = Color(0xFFA6B8C7),
    project: Project,
) {
    val formattedText = formatCode1(text, project)
    var language = SyntaxLanguage.DEFAULT
    var fullCodeInRange = IntRange(0, text.length)
    var codeInRange = IntRange(0, text.length)

    var lastLanguageRange: Int

    val textRanges = findTextRanges(formattedText, "```")

    if (textRanges.isEmpty()) {
        Text(
            text = text,
            modifier = Modifier.padding(all = 10.dp),
            textAlign = TextAlign.Start,
            color = color,
        )
    } else {
        textRanges.forEachIndexed { index, intRange ->
            if (index % 2 == 0) {
                fullCodeInRange = IntRange(intRange.first, text.length)

                SyntaxLanguage.getNames().find { formattedText.substring(intRange.last + 1, intRange.last + 11).substringToEndOfLine() == it }
                    ?.let {
                        lastLanguageRange = intRange.last + 1 + it.length
                        val realLanguageText = formattedText.substring(intRange.last + 1, lastLanguageRange)

                        codeInRange = IntRange(lastLanguageRange, formattedText.length)
                        language = SyntaxLanguage.getByName(realLanguageText) ?: SyntaxLanguage.DEFAULT
                    }
            } else {
                fullCodeInRange = IntRange(fullCodeInRange.first, intRange.last)

                val codeInRangeText = formattedText.substring(codeInRange.first, intRange.first)

                Column(
                    modifier = modifier.padding(vertical = 10.dp)
                ) {
                    Text(
                        text = formattedText.substring(0, fullCodeInRange.first).trimIndent(),
                        modifier = Modifier.padding(horizontal = 10.dp),
                        textAlign = TextAlign.Start,
                        color = color
                    )

                    Text(
                        text = highLightText(codeInRangeText, language),
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 10.dp)
                            .clip(RoundedCornerShape(4.dp))
                            .border(1.dp, Color(0xFF2B2B2B), RoundedCornerShape(4.dp))
                            .padding(6.dp),
                        textAlign = TextAlign.Start,
                        color = color,
                    )

                    Text(
                        text = formattedText.substring(fullCodeInRange.last + 1).trimIndent(),
                        modifier = Modifier.padding(horizontal = 10.dp),
                        textAlign = TextAlign.Start,
                        color = color
                    )
                }
            }
        }
    }
}

fun String.substringToEndOfLine(): String {
    return this.substringBefore("\n", this)
}

fun highLightText(text: String, language: SyntaxLanguage): AnnotatedString {
    val highlights = getCodeHighLights(text, language)

    return AnnotatedString.Builder().apply {
        append(text)

        highlights.forEach {
            val startIndex = it.location.start
            val endIndex = it.location.end
            val colorHighLight = (it as ColorHighlight).rgb

            withStyle(style = SpanStyle(color = Color(colorHighLight))) {
                addStyle(style = SpanStyle(Color(colorHighLight)), start = startIndex, end = endIndex)
            }
        }

    }.toAnnotatedString()
}

fun findTextRanges(documentText: String, target: String): List<IntRange> {
    val textRanges = mutableListOf<IntRange>()
    var index = documentText.indexOf(target)
    while (index >= 0) {
        val range = index until (index + target.length)
        textRanges.add(range)
        index = documentText.indexOf(target, index + target.length)
    }
    return textRanges
}