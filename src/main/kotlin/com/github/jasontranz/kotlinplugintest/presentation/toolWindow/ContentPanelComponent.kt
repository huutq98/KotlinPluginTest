package com.github.jasontranz.kotlinplugintest.presentation.toolWindow

import androidx.compose.runtime.Composable
import androidx.compose.ui.awt.ComposePanel
import com.github.jasontranz.kotlinplugintest.data.mapper.PromptFormatter
import com.github.jasontranz.kotlinplugintest.presentation.action.ChatBotAction
import com.github.jasontranz.kotlinplugintest.presentation.action.ChatBotActionType
import com.github.jasontranz.kotlinplugintest.presentation.component.searchView
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.NullableComponent
import com.intellij.ui.Gray
import com.intellij.ui.JBColor
import com.intellij.ui.OnePixelSplitter
import com.intellij.ui.components.JBLabel
import com.intellij.ui.components.JBPanel
import com.intellij.ui.components.JBScrollPane
import com.intellij.ui.components.panels.VerticalLayout
import com.intellij.util.ui.JBEmptyBorder
import com.intellij.util.ui.JBFont
import com.intellij.util.ui.JBUI
import com.intellij.util.ui.UIUtil
import java.awt.BorderLayout
import java.awt.Rectangle
import java.awt.event.ActionListener
import javax.swing.*
import javax.swing.event.DocumentEvent
import javax.swing.event.DocumentListener
import javax.swing.text.AttributeSet
import javax.swing.text.PlainDocument


class ContentPanelComponent(
    private val chatBotAction: ChatBotAction,
    private val project: Project
) : JBPanel<ContentPanelComponent>(),
    NullableComponent {

    private var progressBar: JProgressBar
    private val myTitle = JBLabel("Conversation")
    private val myList = JPanel(VerticalLayout(JBUI.scale(10)))
    private val mainPanel = JPanel(BorderLayout(0, JBUI.scale(8)))
    private val myScrollPane = JBScrollPane(
        myList, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER
    )
    private lateinit var actionPanel: JPanel
    private lateinit var jScrollPane: JScrollPane

    init {
        val composePanel = ComposePanel()

        val splitter = OnePixelSplitter(true, .98f)
        splitter.dividerWidth = 2

        myTitle.foreground = JBColor.namedColor("Label.infoForeground", JBColor(Gray.x80, Gray.x8C))
        myTitle.font = JBFont.label()

        layout = BorderLayout(JBUI.scale(7), 0)
        background = UIUtil.getListBackground()
        mainPanel.isOpaque = false

        composePanel.setContent {
            ChatPanelView(
                chatBotAction = chatBotAction,
                project = project
            )
        }

        add(composePanel, BorderLayout.CENTER)
//        add(mainPanel, BorderLayout.CENTER)

//        myList.isOpaque = true
//        myList.background = UIUtil.getListBackground()
//        myScrollPane.border = JBEmptyBorder(10, 15, 10, 15)

        splitter.firstComponent = myScrollPane

        progressBar = JProgressBar()
        splitter.secondComponent = progressBar
//        mainPanel.add(splitter)
        myScrollPane.verticalScrollBar.autoscrolls = true
//        addQuestionArea()
    }

    fun add(message: String, isMe: Boolean = false) {
        val messageComponent = MessageComponent(message, isMe)
        val jbScrollPane = JBScrollPane(
            messageComponent,
            ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER,
            ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED
        )

        myList.add(jbScrollPane)
//        progressBar.isIndeterminate = true
        updateUI()
//        scrollToLastItem()
    }

    fun updateMessage(message: String) {
        myList.remove(myList.componentCount - 1)
        val messageComponent = MessageComponent(message, false)
        myList.add(messageComponent)
//        progressBar.isIndeterminate = false
//        progressBar.isVisible = false
        updateUI()
//        scrollToLastItem()
    }

    override fun isNull(): Boolean {
        return !isVisible
    }

    fun updateReplaceableContent(content: String, replaceSelectedText: () -> Unit) {
        myList.remove(myList.componentCount - 1)
        val messageComponent = MessageComponent(content, false)

        val jButton = JButton("Replace Selection")
        val listener = ActionListener {
            replaceSelectedText()
            myList.remove(myList.componentCount - 1)
        }
        jButton.addActionListener(listener)
        myList.add(messageComponent)
        myList.add(jButton)
//        progressBar.isIndeterminate = false
//        progressBar.isVisible = false
        updateUI()
    }


    private fun textAreaExample(): JTextArea {
        val textArea = JTextArea(5, 20) // Specify the preferred rows and columns
        textArea.lineWrap = true // Enable line wrapping
        return textArea
    }

    override fun updateUI() {
        revalidate()
        repaint()
    }
}